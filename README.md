# SUSN: Simple Uniform Semantic Notation
## A hand-editable data format for small personal knowledge bases



## SUSN

SUSN is an experiment in minimal line-oriented syntax for personal semantic notes.

It's based on text files that you edit by hand, and then parse in a programing language in order to generate reports and queries.

susn.js will allow you to parse a SUSN text file into JSON arrays, query and manipulate them, and write them back out to text files.

## Name

I felt that JSON needed a friend.

## Use case

SUSN exists because JSON, Lisp, XML and RDF are not easy to hand-edit in a text file. 

The use-case for SUSN is where you want to take both text and semantic notes in a single format, on a device which only supports a plain text editor.

For susn.js, is also assumed that for querying or reporting your notes, you have a Javascript installation available (for example, Node.js in a terminal window, or a HTML page with Javascript that is able to read your text file).

These two assumptions are generally true on Android devices where Termux is available, and on Windows and Linux notebooks.

However SUSN as a format does not rely on Javascript. It should be implementable on any small system with a concept of both strings, and either arrays or lists.

## Syntax example (A syntax)
```
-module 0
--title Tomorrow
--novas 0
--timeline 1982
--year 2014
--youtube https://www.youtube.com/playlist?list=PLMRcNPXFEbAFnu_zQkn_UPVY-yMS78iz4
--book It's 1982. One of the 1982s, at least. This one is a little faster...
--track Singing In The 80's
---by The Monitors
----country Australia
----artist Terry McCarthy
----artist Mark Moffat
---year 1980
---single
---cast Jack
----tag debut
---book Jack is a soldier with shadows.
---tag coldopen
---tag memory
---lyrics
---- I didn't know if I wanted to laugh or cry
---- The night that disco died
---- The laser lights were kicking off my dancing shoes
---- But there was nobody dancing inside
---- Got high with the song of doom
---- The moon was listening in
----
---- What will we be singing in the '80s?
---- What should we be singing now?
---- What will we be singing in the '80s?
---- What should we be singing now?
```

## Syntax example (B syntax)

```
[module 0
title Tomorrow
novas 0
timeline 1982
year 2014
youtube https://www.youtube.com/playlist?list=PLMRcNPXFEbAFnu_zQkn_UPVY-yMS78iz4
book It's 1982. One of the 1982s, at least. This one is a little faster...
[track Singing In The 80's
[by The Monitors
country Australia
artist Terry McCarthy
artist Mark Moffat
]
year 1980
single
[cast Jack
tag debut
]
book Jack is a soldier with shadows.
tag coldopen
tag memory
[lyrics
. I didn't know if I wanted to laugh or cry
. The night that disco died
. The laser lights were kicking off my dancing shoes
. But there was nobody dancing inside
. Got high with the song of doom
. The moon was listening in
.
. What will we be singing in the '80s?
. What should we be singing now?
. What will we be singing in the '80s?
. What should we be singing now?
]
]
]
```
## Syntax example (C syntax)

```
module 0
 title Tomorrow
 novas 0
 timeline 1982
 year 2014
 youtube https://www.youtube.com/playlist?list=PLMRcNPXFEbAFnu_zQkn_UPVY-yMS78iz4
 book It's 1982. One of the 1982s, at least. This one is a little faster...
 track Singing In The 80's
  by The Monitors
   country Australia
   artist Terry McCarthy
   artist Mark Moffat
  year 1980
  single
  cast Jack
   tag debut
  book Jack is a soldier with shadows.
  tag coldopen
  tag memory
  lyrics
   . I didn't know if I wanted to laugh or cry
   . The night that disco died
   . The laser lights were kicking off my dancing shoes
   . But there was nobody dancing inside
   . Got high with the song of doom
   . The moon was listening in
   .
   . What will we be singing in the '80s?
   . What should we be singing now?
   . What will we be singing in the '80s?
   . What should we be singing now?
```


## Usage (Node.js)

Suppose we have a SUSN document `kb.susn.txt` and a running Node.js installation. And you have the knowledge base and susn.js in the working directory. And you have sensibly coded your SUSN file as UTF8 with LF linebreaks. Then an interactive invocation protocol is:

```
susn = require("./susn.js")
q = susn.q
kb = susn.parse(fs.readFileSync("kb.susn.txt","utf8").split("\n"))
q.wr(kb, ...pipeline)
```
where "pipeline" is a series of q. functions. Then you switch to your text editor to make changes to your file, and rerun 'kb =' to reload and 'q.wr' to query. 

The Node.js REPL makes it very easy to repeat this with readline and up-arrow, and you can run Node under Windows, Linux, or under Termux on any Android device. No idea what is available in the way of interactive Node terminal environments for iOS, but I assume MacOS on the desktop at least hasn't banned Node yet.

(Termux is only barely tolerated on Android in 2024; it has to be installed using F-Droid. And you have to do a complicated permissions dance to grant Termux rights to your filesystem, and then make sure to `cd /storage/emulated/0` to actually get to that filesystem and access your actual, local files (which can be copied to and from your device over a USB link). Android will also occasionally at random do a complete reset of your Termux environment, killing your running Node session. Other than that, it works pretty well.)

## Structure

SUSN is based on nested terms. 

A term is a single text line (which can contain any character except a linebreak), called its "head", followed by any number of sub-terms, called its "items" (the list of items being the "body").

A term logically means "a statement, and a set of statements about that statement".

The internal data model is an array where the first element is a string, and each other element is an array.

A term head is usually - but doesn't always have to be - further interpreted as a significant "keyword" followed by a list of words.

## Syntax

There are three display or parsing syntaxes.

Syntax A indents lines with `-` and uses `.` to escape a line beginning with the indent character. Lines with no indent character are ignored.

Syntax B brackets lines. A line starting with `[` begins a new term, a line starting with `]` closes a term, and a line starting with `.` escapes a term with no subterms that starts with one of these characters. All other lines are treated as a term with no subterms.

Syntax C is indented, like syntax A, but with spaces. `.` is used to escape blank lines or lines starting with a space. Actually blank lines (including all-space lines) are ignored. A non-blank line with no indent is counted as a line at the top level of indentation (the equivalent of one dash for syntax A).

(In Node.js particularly, it is pleasant to add ANSI colours when displaying SUSN text: white for the special characters and green for the text, which follows the same visual convention as the normal Node REPL's display of arrays).

A SUSN term can be arbitrarily large (though there is a practical maximum size based on the ease of loading and searching within a text editor) and recursively contains multiple terms and multiple levels of nesting. Sequence is significant, and elements can be repeated. This gives it more structure than a SQL database, and makes it more like a highly simplified XML document.

## Design constraints

In 2024, despite over 25 years of the open source and open data movements, we face a widespread and growing problem of centralised cloud-based apps that invade user privacy. 

Users would like to be able to create, store, query and share small personal knowledge graphs without a cloud service being involved.

One of the few data formats which remain locally usable, even on small mobile devices, storable for a long time, and easily transferred between systems, is plain ASCII or Unicode text.

However most plain text data formats are mostly not designed for direct human use but for serialization by machine, only usable through an app. Apps are not guaranteed to always be on your device, or to work in your interests.

Therefore:

SUSN is a plain text knowledge representation format designed for direct human use, unmediated by an app. 

It is intended to be rapidly entered and hand-edited in a very basic editor, even one which does not provide indentation. It is also intended to be transmitted through text messaging systems which preserve line breaks, but may mangle leading whitespace. 

SUSN is also based on an RDF, JSON or SQL like concept of small assertions each describing an individual aspect of a complex knowledge graph, and where the units being described are English text that may contain syntax-breaking punctuation (eg: brackets, slashes, quotation marks, spaces). 

Unlike RDF graphs, JSON objects, or SQL tables (but like XML), SUSN terms are inherently ordered and can contain repetition. 

## Extremely similar things

Steven Obua's "Recursive Text" (https://practal.com/recursivetext/) 

Breck Yunits' "Scroll Notation" (https://faq.scroll.pub/)

However, SUSN currently is not quite directly compatible with Recursive Text or Scroll Notation, because its use of tangible rather than whitespace indentation means term heads can be almost arbitrary strings (ie, they can begin with whitespace, but cannot contain linebreaks). RX or Scroll lines cannot begin with whitespace. 

In particular, this difference affects the simple microformat used in SUSN for embedding raw text: starting a term head with a space. These terms will not directly map across to RX or Scroll without some kind of escaping convention.


## Inspirations and somewhat similar things

JSON (SUSN maps to JSON arrays, or any other data model with strings and arrays)

RDF (SUSN was originally based on one-line assertions)
Markdown (SUSN can be used for text notes as well as data)

## Update 2024-08-20

Syntax C is back, this time with . as an escape char (A escape is now . again to synchronise all three escape chars), and zero indent is the top level. For the first time I think this is a space-indent syntax I'm happy with. It should be completely round-trippable between A and B syntaxes. 

Blank lines in the new C syntax are ignored, while actually-blank lines in the data are escaped. 

## Updated 2024-08-19

Well, that didn't last. Syntaxes A and B are back, but A now uses - instead of / as indent char, and has an escape char (for edge cases). As before, `susn.syntax` holds the current syntax; q.wr, q.wb, q.load, q.save all use the currently selected syntax. q.loada/loadb and q.savea/saveb allow loading and saving documents in the non-current syntax.

The reason this time: I wanted to encode a line-and-box graph (one of the original use cases) and it was just too painful using B syntax but looked perfect in A. There are enough tradeoffs between A and B that it's still worth keeping B around (B is around 10% shorter, and is much better for entering bulk raw text).

Hyphen as the indent makes A syntax feel much more Markdown-y, as a natural "indented list" format. 

Syntax C remains gone. I can't make it work with leading-blank lines, and I really want those.

## Updated 2024-07-29

SUSN now only supports one syntax again: B syntax.

Multisyntaxes were reverted in the interests of simplicity. It was an interesting experiment, but the ambiguities inherent in handling whitespace and difficulties dealing with raw text terms seem to be pushing SUSN in the wrong direction for what it is.

SUSN needs to be as small as possible, and B syntax currently produces the smallest text files and the simplest parser. And while it's not the most beautiful on all measures, it is easiest syntax to comprehend what's going on.

The word "memo" is now "term", reflecting a more general logic interpretation of SUSN documents.

The SUSN.Q functional query language has also been tweaked. The word "each" is now "with", which parses better in English. The word "key" is now "is". Almost all basic query words (is, has, not, get, with) now take an "any" (list of alternative queries to be composed in order) which streamlines query writing.

The query pipeline is getting clearer: "is", "has", "not" to filter terms; "any", "with" or "get" to rewrite them. Filtering and rewriting are roughly equivalent to relational selection and projection.

Still searching for a good general solution for joining / matching. Probably some form of arrow function.


## Updated 2024-07-13

Added stricter parse errors:

B syntax now rejects the whole document if the text after a close bracket does not match the open bracket. This lets the user enforce strict XML-like tag name checking, if they wish. The default behaviour of the SUSN.B syntax writer is still to not add any text after a close bracket.

A and C syntaxes now reject the whole document if any non-blank line does not start with the indent character.

This is to make SUSN.C syntax safer against whitespace mangling. A line that starts with no indent (very easy to accidentally create in a hand-authored file) would otherwise be silently ignored, which would result in a data structure that merged all of the level 2 items together - never a correct outcome. If we throw an error on encountering a single non-space-indented line, we can be reasonably sure that the input document a) was not accidentally mistyped with no indent and also b) did not transit through a whitespace-deleting system.

It would seem more natural to interpret non-space-indented lines as nesting depth 1 (with one space being depth 2, etc). But if we do this, we get a serious non-resolvable ambiguity over the meaning of a completely blank line. Should it be ignored as user-specified whitespace? Or does it encode a blank item at the top level of the structure - which would destroy the nesting structure if added accidentally? The likelihood of the user adding blank lines thinking they mean whitespace (as in every other language) seems very high. Therefore space-prefixing every line, and then ignoring completely blank lines, seems to be the only safe option. 

With these changes, it might be possible to guess the syntax of an unknown document: attempt to parse as A or C, then fall back to B. But since almost every possible text document (including malformed A or C syntax) can parse (incorrectly) as B, it's still safer to specify the syntax and then reject if it fails that specific syntax.


## Updated 2024-07-10 - Multi-syntaxes

SUSN now has three syntaxes: SUSN.A, SUSN.B, and SUSN.C. A recommended file extension is .susn.a.txt etc.

SUSN.A is the original "armored indent" with / characters

SUSN.B is the new bracket syntax, but with the dot "escape" character removed (since its main use was for heads with leading whitespace)

SUSN.C is a spaced syntax - same as SUSN.A except the indent character is a space

All write and parse functions now need to specify the syntax, because it is not always possible to unambiguously detect the syntax of an unknown file. (Everything looks like valid SUSN.B, for example.)

SUSN.C is there to be roughly compatible with Steven Obua's "Recursive Text" (https://practal.com/recursivetext/) and Breck Yunits' "Scroll Notation" (https://faq.scroll.pub/), which are similar block-structured text concepts using whitespace indent.

As a result of introducing SUSN.C syntax, SUSN Heads (as opposed to the raw syntax lines which contain them) are now required to NOT start with any whitespace. All Heads are run through trim() to remove leading and trailing whitespace on parse and write.

One non-obvious quirk of SUSN.C is that, because it's a drop-in for SUSN.A syntax, all lines MUST begin with an initial space or they are rejected. This is a happy accident because it adds an important safety factor: without requiring leading spaces, a blank line would reset the indent to level one, which would generally be a disaster. This also means that a SUSN.C document which has been mangled by running through a text transmission system which trims leading spaces, will be entirely rejected rather than being partially accepted. This also seems safer.

Another non-obvious quirk of SUSN.C is that it is possible to encode lines which look identically blank, but which because they have different numbers of spaces, represent very different memos. (Ie, with blank items at different locations in the tree). The upside is that (assuming the system is not trimming whitespace) you still get a correctly machine parseable and writeable representation of the memo. The downside is that a human can't visually detect the structure. This is, unfortunately, one of the side effects we get with whitespace indentation, and there is no easy way to improve this situation.

However, if you as a human don't care about where exactly in the tree the blank items are (you can figure it out in a text editor, with some difficulty, by pressing End on each line and checking where the cursor goes) then SUSN.C is much more readable than the other two syntaxes.

The Novas0 demonstration file has been uploaded in all three syntaxes. 

Note, the "dot prefix" notation used in Novas0 for "raw text", is now not a core SUSN syntax: it's more like a convention or "micro-format" layered over SUSN. The advantage of using a prefix convention like this is just that it makes querying a little easier - because it gives even raw text lines an initial keyword - and also visually delimits raw text when you're eyeballing a file. But you could put raw text straight in as a head anywhere, if you wanted to.


## Updated 2024-06-21 - SUSN 7

SUSN 7 is a complete rewrite of SUSN, with two major changes: the concept of noun and verb are gone, with all heads being just a line of text, and the syntax is no longer indent-based but uses open and close brackets. The query language is rewritten as well. 

The good news is it's now a lot smaller.

Gemini (SUSN is line-oriented and minimalist)

recfiles (SUSN is also a line-oriented text database language)

XML (SUSN is sort of tag-based and document-based)

S-expressions (SUSN is line-based rather than token-based, but has listlike structure)

## SUSN.Q query language

SUSN.Q is a minimalist "query language" (a set of functions) for querying SUSN memos, under the susn.q namespace. It is experimental and evolving. Current functions include:

`q.load(filename)` - load a SUSN file

`q.wha(...query)` (`whb` `whc`) - construct a query and write the result, including head, in ANSI colored SUSN-A/B/C syntax


`q.w(...query)` (`wb` `wc`) - write just the body of a query result in SUSN syntax

`q.save(filename, ...query)` - save the result of a query as SUSN to a file

`q.from(term)` - load a term, replacing the current term in the pipeline

`q.with(...queries)` - select each term and replace its contents with the queries

`q.has(...queries)` - select only those terms with contents matching one of the querues

`q.get(...queries)` - select the contents of all terms matching any query

`q.is(...keywords)` - (or a string for short) - include only those terms whose head matches the keyword (the idea of "keyword" replaces and extends the older SUSN idea of "noun")

`q.isexact` - like q.is but include only exact matches of the term head

`q.not(...queries)` - select only those terms NOT matching any of the queries

`q.item(...ns)` - include only the nth items in the memo, in the order specified

`q.any(...queries)` - run multiple queries, including all of the items in their results, in order

`q.merge` - combine items with duplicate heads, moving the items of later-mentioned ones into the first occurrence, while preserving the overall order

`q.sort` - sort the items by head, or by any other sorting function

`q.count` - replace all items with a single item which is the previous count of items
`

## Future directions for development

Find a general join/match solution.

Changing head from a raw string to an array of tokens (interned strings and/or numbers)









