/*
-project SUSN: Simple Uniform Semantic Notation
--by Nate Cull
--v 2024-09-30
--- q.just is now q.put (to match q.get)
--- q.sort updated to use susn.sort
--- syntaxes are now stateless:
---- susn.syntax and susn.syn removed
---- q.wb is now q.wa/b/c ("write")
---- q.wr is now q.wha/b/c ("write with head" - the less usual case when querying)
--- new susn method-like functions:
---- susn.term() now takes body as ordinary second parameter
----- this is because large arrays can't be deconstructed as function arguments
---- susn.emptyTerm() for terms with empty head
---- susn.headEquals() tests equality of heads
---- susn.size() abstracts testing size of body
---- susn.sort() sorts body while preserving head
---- susn.item() extracts items (starting with 1 for first body item, as for array indexes where 0 is head)
--- B syntax changes
---- now writes first character of head as close tag
---- prefixes escape char for all non-bracket heads (selectable as susn.b.alwaysWriteEscape)
---- left whitespace also trimmed while parsing
---- this is to make syntax visually aligned
---- slightly visually noiser but also more regular
---- also more robust against deleted lines
---- for now, B syn will accept non-prefixed lines but could reject
---- intended for non-indented use but can be indented if needed
--- q functions
---- each, withAlso, just, sameHeadAs, setKey
---- isexact is now isExact
---- head0,head1,head2,head3 for headline levels
--- susn.syn function for switching syntaxes
--v 2024-08-19
--- C syntax restored
--- revised C syntax now uses . escape and includes zero indent
--- C syntax can now handle heads with leading spaces
--- A syntax escape is now .
--v 2024-08-17
--- A syntax escape is now /
--v 2024-08-16
--- restored A and B syntaxes
--- revised A syntax now uses - for indent
--v 2024-07-29
--- "susn" module name no longer capitalized
--- "memo" is now "term"
--- removed syntaxes A and C, only using B syntax
--- reason: simplicity more important than compatibility
--- heads are now arbitrary strings again and are not trimmed
--- empty terms are written with dot escape
--- reworked susn.q:
---- q is now the pipe function as well as the namespace
---- q.save now writes just the body of a term
---- q.key is now q.is
---- q.each is now q.with
---- q.has,get,not,with now do an implicit any
---- q.hasnt,q.without
---- these changes make queries simpler since usually any is needed
--v 2024-07-14
--- syntax errors
--v 2024-07-13
--- reject document in A and C syntax if it contains non-indents
--- reject document in B syntax if close brackets are mismatched
--v 2024-07-10-1
--- added multiple syntaxes
--- SUSN.A is anchored indents 
--- SUSB.B is brackets
--- SUSN.C is space indents
--- heads are now auto-trimmed of leading and trailing whitespace
--- dot is no longer escape
--v 2024-06-23-2
--- refactored q using q.mapItems
--- added q.load, q.save
--- added susn.head, susn.body
--- q.sortItems is now q.sortItemsBy
--- susn.runPipe is now susn.pipe
--- refactored q.merge with q.mergeItemsBy and q.mergeByHead
--v 2024-06-23-1
--- q.pipe is now q.all
--- q.hasnt short for q.not(q.has))
*/



"use strict"

let susn = {}
module.exports = susn

// "Methods" for a SUSN term, but without yet being a Javascript object
// we might make a term a class/prototype at some point
// but for now let's just use functions for the method-like stuff

// This is because it's quite convenient in Node for a SUSN term to be just a vanilla array
// and also in case we want to port to some other language which isn't object-oriented

// Create a new SUSN term

// the head of a term (element 0) is just a string
// the body of a term (the items) each must be terms

susn.term = (head="", body=[]) => [head, ... body]
susn.emptyTerm = body => susn.term("",body)
susn.isTerm = x => Array.isArray(x) &&  susn.isHead(x[0])
susn.isHead = x => typeof x == "string"
susn.head = term => term[0]
susn.body = term => term.slice(1)
susn.size = term => term.length-1
susn.item = i => term => i>0 && i<=susn.size(term) ? term[i] : susn.term()


// Compare two heads
// Straightforward if the head is a string, will be tricker if head is an array of words

susn.headEquals = (head1,head2) => head1 == head2

// Sort a term while keeping the head in first place
// Deconstructing and reconstructing an array like this might be slow
// but it's reliable

susn.sort = sorter => term => 
	susn.term(susn.head(term),susn.body(term).toSorted(sorter))

// The "key" is the initial word of the head (currently represented as string)
// We often want to test for a key, or rewrite the key

susn.hasKey = k => term => 
	susn.head(term).startsWith(k) && [" ",undefined].includes(susn.head(term)[k.length])
	
susn.setKey = k => term => 
	[ [k, ... susn.head(term).split(" ").slice(1)].join(" "), ... susn.body(term)]
	
// That's the end of the method-like functions

susn.parseError = (line,err) => ["Parse error at line",line,err]


susn.ansi = {
	green:"\x1b[32m", 
	white: "\x1b[37m" 
}


// Selectable syntax

susn.a = {type:"a"}
susn.b = {type:"b"}
susn.c = {type:"c"}

/*
susn.syntax = susn.b

// Convenience function for switching syntaxes

susn.syn = function(s) {
	if (!susn[s] || susn[s].type != s) {
		throw "Unknown syntax: " + s
	}
	susn.syntax = susn[s]
}
*/

// A syntax (indents)

susn.a.indentChar = "-"
susn.a.escapeChar = "."
susn.a.escapableChars = susn.a.indentChar + susn.a.escapeChar

susn.a.escape = str => str == "" || !susn.a.escapableChars.includes(str[0]) ? str : susn.a.escapeChar + str
susn.a.unescape = str => str[0] == susn.a.escapeChar ? str.slice(1) : str

susn.a.getIndent = (line) => {
	let depth=0
	for (depth=0;depth<line.length;depth++) {
		if (line[depth]!=susn.a.indentChar) { break }
	}
	return depth
}

susn.a.parse = (lines=[],stack) => {
	if (stack==undefined) { stack = [susn.term()] }
	let l = 0
	lines.forEach(line => {
		l++
		
		// Find the indent of the line
		let depth = susn.a.getIndent(line)
		let str = susn.a.unescape(line.slice(depth))
		
		// Ignore non-indented lines
		
		if (depth<1) { return } 
		
		// Close any open terms at depth
		while (depth<stack.length) { stack[stack.length-2].push(stack.pop()) }
		// Open new terms to depth
		while (depth>stack.length) { stack.push(susn.term()) }
		// Add new term
		stack.push(susn.term(str))
	})
	
	// Close all open terms
	
	while (stack.length>1) { stack[stack.length-2].push(stack.pop()) }
	
	return stack[0]
}

susn.a.writeBody = (term,lines=[],depth=1) => {term.forEach(m=>susn.a.write(m,lines,depth)); return lines}

susn.a.write = (term,lines=[],depth=1) => { 
	if (! susn.isTerm(term)) { return } 
	let head = susn.head(term)
	lines.push(susn.a.indentChar.repeat(depth) + susn.a.escape(head))
	susn.a.writeBody(term,lines,depth+1)
	return lines
}

susn.a.color = str => {
	let depth = susn.a.getIndent(str)
	let i = str.slice(0,depth)
	let h = str.slice(depth)
	if (h[0]==susn.a.escapeChar) {
		return susn.ansi.white + i + susn.a.escapeChar + susn.ansi.green + h.slice(1)
	} else {
		return susn.ansi.white + i + susn.ansi.green + h
	}
}

// B syntax (brackets)

susn.b.openChar = "["
susn.b.closeChar = "]"
susn.b.escapeChar = "."
susn.b.syntaxChars = susn.b.openChar + susn.b.closeChar + susn.b.escapeChar
susn.b.escapableChars = " " + susn.b.syntaxChars


susn.b.parse = (lines,stack) => {
	if (stack==undefined) { stack = [susn.term()] }
	let l = 0
	lines.forEach(line => {
		line = line.trim()
		l++
		let c = line[0]
		if (c==susn.b.openChar) {
			// Open a term
			stack.push(susn.term(line.slice(1)))
		} else if (c==susn.b.closeChar) {
			// Close a term
			if (stack.length <2) { 
				throw susn.parseError(l, "unexpected " + susn.b.closeChar)
			} else if (!susn.head(stack[stack.length-1]).startsWith(line.slice(1))) {
				throw susn.parseError(l, "mismatched " + line)
			} else {
				stack[stack.length-2].push(stack.pop())
			}
		} else if (c==susn.b.escapeChar) {
			// Add an escaped term
			stack[stack.length-1].push(susn.term(line.slice(1)))
		} else {
			// Add an empty ordinary term
			stack[stack.length-1].push(susn.term(line))
		}
	})

	if (stack.length > 1) { 
		throw susn.parseError(l, "no final " + susn.b.closeChar)
	}
	return stack[0]
}

// Write a term

susn.b.writeBody = (term,lines=[]) => {term.forEach(m=>susn.b.write(m,lines)); return lines}

//susn.b.closeTag = head => (head[0] ?? "").trim()
//susn.b.closeTag = head => head[0]==" " ? head.slice(0,2) : head.slice(0,1)
susn.b.closeDepth=1
susn.b.closeTag = head => head.slice(0,susn.b.closeDepth)
susn.b.alwaysWriteEscape = true

susn.b.write = (term,lines=[]) => { 
	if (!susn.isTerm(term)) { return lines }
	let head = susn.head(term)

	if (susn.size(term)>0){
		lines.push(susn.b.openChar+head)
		susn.b.writeBody(term,lines)
		lines.push(susn.b.closeChar  + susn.b.closeTag(head) )
	} else if (susn.b.alwaysWriteEscape || head == "" || susn.b.escapableChars.includes(head[0])) {
		lines.push(susn.b.escapeChar + head)
	} else {
		lines.push(head)
	}
	return lines
}

susn.b.color = str => susn.b.syntaxChars.includes(str[0]) ? susn.ansi.white + str[0] + susn.ansi.green + str.slice(1) : susn.ansi.green + str


// C syntax (space indents)

susn.c.indentChar = " "
susn.c.escapeChar = "."
susn.c.escapableChars = susn.c.indentChar + susn.c.escapeChar

susn.c.escape = str => str == "" ? susn.c.escapeChar : !susn.c.escapableChars.includes(str[0]) ? str : susn.c.escapeChar + str
susn.c.unescape = str => str[0] == susn.c.escapeChar ? str.slice(1) : str

susn.c.getIndent = (line) => {
	let depth=0
	for (depth=0;depth<line.length;depth++) {
		if (line[depth]!=susn.c.indentChar) { break }
	}
	return depth
}

susn.c.parse = (lines=[],stack) => {
	if (stack==undefined) { stack = [susn.term()] }
	let l = 0
	lines.forEach(line => {
		l++
		
		// Find the indent of the line
		let indent = susn.c.getIndent(line)
		// Ignore unescaped blank lines
		if (indent==line.length) { return } 
		
		let depth = indent +1 // Treat lines with no indent as indent 1
		let str = susn.c.unescape(line.slice(indent)) 
		
		// Ignore unescaped blank lines
		
	
		
		// Close any open terms at depth
		while (depth<stack.length) { stack[stack.length-2].push(stack.pop()) }
		// Open new terms to depth
		while (depth>stack.length) { stack.push(susn.term()) }
		// Add new term
		stack.push(susn.term(str))
	})
	
	// Close all open terms
	
	while (stack.length>1) { stack[stack.length-2].push(stack.pop()) }
	
	return stack[0]
}

susn.c.writeBody = (term,lines=[],depth=1) => {term.forEach(m=>susn.c.write(m,lines,depth)); return lines}

susn.c.write = (term,lines=[],depth=1) => { 
	if (! susn.isTerm(term)) { return } 
	let head = susn.head(term)
	lines.push(susn.c.indentChar.repeat(depth-1) + susn.c.escape(head))
	susn.c.writeBody(term,lines,depth+1)
	return lines
}

susn.c.color = str => {
	let depth = susn.c.getIndent(str)
	let i = str.slice(0,depth)
	let h = str.slice(depth)
	if (h[0]==susn.c.escapeChar) {
		return susn.ansi.white + i + susn.c.escapeChar + susn.ansi.green + h.slice(1)
	} else {
		return susn.ansi.white + i + susn.ansi.green + h
	}
}


// Query language
// based on pipes of functions that transform terms - mostly just the body



// Preprocess a query pipe, expanding syntax sugar as necessary
// a string becomes q.is(string)
// a term-shaped array becomes q.from(term)

susn.makePipe = pipe => 
	pipe.map(f => 
		typeof f == "string" ? q.is(f) 
		: susn.isTerm(f) ? q.from(f) 
		: f )

// Run a query pipe, applying each function in turn to an initial term

susn.pipe = pipe => term => 
	susn.makePipe(pipe).reduce((m,f)=>f(m),term)

// A pipe is so useful that we'll make it the shortest name - q(...)
// I'm really sorry about this trick - it's not nice at all
// it's relying on Javascript functions also being objects

susn.q = (...query) => susn.pipe(query)
let q = susn.q

// Run a pipe against an empty term (ie for interactive use)

q.select = (...query) => q(...query)(susn.term())

// Replace the current term with a new one

q.from = term => m => term

// Map each item to zero or more new items, keeping head intact

q.mapItems = f => term =>
	term.flatMap((m,n) => n==0 ? [m] : f(m))

// Reject all items from this term which have a non-empty subquery
// empty means rejects everything

q.not = (...queries) => term => queries.length==0 ? susn.term() : 
	q.mapItems(m => (q.any(...queries)(term)).includes(m) ? [] : [m])(term)


// Run multiple pipes against this term, and append the results together
// An empty set of pipes returns just the head of the term

q.any = (...queries) => term =>  
	[susn.head(term), ... queries.flatMap(a=>susn.body(q(a)(term)) )]


// Select all items whose heads match (the initial words of) any of the keys

q.is = (...keys) =>
	q.mapItems(m=>keys.some(k => susn.hasKey(k)(m)) ? [m] : [])
	
//q.is = key => q.isAny(key)

// Select all items whose heads exactly match any of the keys
	
q.isExact = (...keys) =>
	q.mapItems(m => keys.some(k => susn.headEquals(k,susn.head(m))) ? [m] : [])

// Select all items whose heads match the head of a term
	
q.isHead = term => q.isExact(susn.head(term))
	
// Rewrite the key of all items (useful if the key represents a relation)

q.setKey = k => q.mapItems(i => [susn.setKey(k)(i)])
	
// Select all items whose bodies produce a non-empty subquery (ie, filter)
// empty means has anything
	
q.has = (...queries) => queries.length==0 ? q.has(q()) : 
	q.mapItems(m=> susn.size(q.any(...queries)(m))>0 ? [m] : [])
	
q.hasnt = (...queries) => q.not(q.has(...queries))
	
	
// Select the contents of all items after applying a subquery (ie, flatMap)
// empty means gets everything

q.get = (...queries) => queries.length==0 ? q.get(q()) :
	q.mapItems(m => susn.body(q.any(...queries)(m)))

// Apply a subquery to the bodies of all items (ie, map)
// empty means gets nothing

q.with = (...queries) =>
	q.mapItems(m=>[q.any(...queries)(m)])
	
// Reject subqueries for bodies of all items

q.without = (...queries) => q.with(q.not(...queries))

// add results of a set of queries to a term

q.withAlso = (...queries) => q.with(q.any(q(), ...queries))




// Select term items by number
// out of range item numbers become an empty term

q.item = (...ns) => term => 
	susn.term( susn.head(term), ns.map(susn.item) )

// Apply a function to each item in a term, as its own pipeline, appending the results
// Used for binding a variable for matching/joining, eg:
// called as q.each(itemName=>q-function-which-references-itemName)

q.each = f => q.mapItems(i=>susn.body( f(i)(susn.emptyTerm([i])) ))



// Wrap a term in another term (with empty head)
// Useful for introducing a saved q.each term into a pipeline 

q.put = term => susn.emptyTerm([term])




// Sort all items in this term - by default, by a text sort on their heads
//q.sortItemsBy = sorter => term => term.toSorted( (m1,m2) => susn.isHead(m1) ? -1 : comparator(m1,m2) )

q.sortByHead = (term1,term2) => { 
	let h1 = susn.head(term1)
	let h2 = susn.head(term2)
	return h1<h2 ? -1 : h1>h2 ? +1 : 0
}
q.sort = susn.sort(q.sortByHead)

// Merge all items in this term (unifying them based on exact match of their head) without changing their order

q.mergeByHead = (term1,term2) => susn.headEquals(susn.head(term1),susn.head(term2))
q.mergeItemsBy = comparator => term => term.reduce( (merged,m) => {
	let i = merged.findIndex(x => susn.isTerm(x) && comparator(x,m))
	if (i<0) { 
		merged.push(m) 
	}  else {
		merged[i] = merged[i].concat(susn.body(m))
	}
	return merged
},[])
q.merge = q.mergeItemsBy(q.mergeByHead)


// Replace body of this term with one item, which is the count of the items

q.count = term => susn.term(susn.head(term), [susn.term(susn.size(term).toString())])

// Replace a term with its first item - not sure if we need this

q.zoom = term => term[1]



// Reduce a term by headline levels - 0 is empty, 1 is headline, etc
q.head0 = q.is()
q.head1 = q.with()
q.head2 = q.with(q.head1)
q.head3 = q.with(q.head2)




// Node.js console specific tools

// Pretty console printing 


// Pretty-print a pipe


q.whX =  (syntax) => (...query) => { syntax.write(q.select(...query)).forEach(line=>console.log(syntax.color(line))) }

//q.wh = q.whX(susn.syntax)

q.wha = q.whX(susn.a)
q.whb = q.whX(susn.b)
q.whc = q.whX(susn.c)

q.wX =  (syntax) => (...query) => { syntax.writeBody(q.select(...query)).forEach(line=>console.log(syntax.color(line))) }

//q.w = q.wX(susn.syntax)

q.wa = q.wX(susn.a)
q.wb = q.wX(susn.b)
q.wc = q.wX(susn.c)

// Load a file

susn.smartSplit = str => str.includes("\r\n") ? str.split("\r\n") : str.split("\n")
susn.smartFilename = (file,syn="a") => file.includes(".") ? file : file + ".susn." + syn + ".txt"

q.loadX = syntax => file => syntax.parse(susn.smartSplit(fs.readFileSync(susn.smartFilename(file,syntax.type),"utf8")))

//q.load = q.loadX(susn.syntax)
q.loada = q.loadX(susn.a)
q.loadb = q.loadX(susn.b)
q.loadc = q.loadX(susn.c)



// Run a query and save it to a file

q.saveX = syntax => (file,...query) => fs.writeFileSync(susn.smartFilename(file,syntax.type),syntax.writeBody(q.select(...query)).join("\n"))

//q.save = q.saveX(susn.syntax)

q.savea = q.saveX(susn.a)
q.saveb = q.saveX(susn.b)
q.savec = q.saveX(susn.c)